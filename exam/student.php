<!DOCTYPE html>
<html>
    <head>
        <title>Students</title>
    </head>
    <body>
    <!-- php code for get key and value of an array from studentInfo.txt -->
    <?php
    $file = fopen("studentInfo.txt", "r+");
    $studentInfo = array();
    while (!feof($file)){
        $handle = fgets($file);
        $explode = explode(" ", $handle);
        $studentInfo[$explode[0]] = intval($explode[1]);
    }
    fclose($file);
    //print_r($studentInfo);
    // Result processing
    function resultProcess($x){
        if($x>=80){
            echo "Number is: ".$x."<br/>";
            echo "Result is: A+";
        }elseif ($x<80 && $x>=70){
            echo "Number is: ".$x."<br/>";
            echo "Result is: A";
        }elseif ($x<70 && $x>=50){
            echo "Number is: ".$x."<br/>";
            echo "Result is: Passed";
        }elseif ($x<50){
            echo "Number is: ".$x."<br/>";
            echo "Result is: Failed";
        }
    }
    ?>
    <!-- first part of the page -->
    <strong>Student's ID List:</strong>
    <?php studentList($studentInfo)?><br/>
    <label>Student Number: </label><?php studentNumber($studentInfo) ?><br/>
    <label>Student who get higher than 80: </label><?php higherThanEighty($studentInfo) ?>
    <br/>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label>Enter student ID: </label><br/>
        <input type="text" name="name"/>
        <br/><input type="submit" value="Result"/>
    </form>
    <?php
        function studentNumber($x){
            $studentNumber = 0;
            foreach ($x as $k => $v){
                $studentNumber++;
            }
            echo $studentNumber;
        }
        function higherThanEighty($x){
            $count = 0;
            foreach ($x as $k => $v){
                if ($v>=80){
                    $count++;
                }
            }
            echo $count;
        }
        function studentList($x){
            foreach ($x as $k => $v){
                echo "<br/>";
                echo "<strong>".$k."</strong>";
            }
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $studentId = $_POST['name'];
            if ($studentId == NULL) {
                echo "Please Enter an ID";
            } elseif (array_key_exists($studentId,$studentInfo)) {
                foreach ($studentInfo as $k => $v){
                    if ($studentId == $k){
                        resultProcess($v);
                        break;
                    }
                }
            } else {
                echo "ID NOT FOUND";
            }
        }
    ?>


    </body>
</html>
