<?php
class calc {
    public $num1;
    public $num2;
    public function setValue($x='',$y=''){
        $this->num1 = $x;
        $this->num2 = $y;
    }
    public function Add(){
        return $this->num1 + $this->num2;
    }
    public function Sub(){
        return $this->num1 - $this->num2;
    }
    public function Mul(){
        return $this->num1 * $this->num2;
    }
    public function Div(){
        return $this->num1 / $this->num2;
    }

}
$n1 = $_POST['num1'];
$n2 = $_POST['num2'];
$obj = new calc();
if (!empty($n1) && !empty($n2)){
    $obj->setValue($n1,$n2);
    if (isset($_POST['add'])){
        echo $obj->Add();
    }elseif (isset($_POST['sub'])){
        echo $obj->Sub();
    }elseif (isset($_POST['mul'])){
        echo $obj->Mul();
    }elseif (isset($_POST['div'])){
        echo $obj->Div();
    }
}else {
    echo "Please Complete All fields";
}
