<?php
class result {
 public $math;
 public $eng;
 public $ben;
 public $phy;
 public function setValue($a,$b,$c,$d){
     $this->math = $a;
     $this->eng = $b;
     $this->ben = $c;
     $this->phy = $d;

 }
 public function getResult()
 {
     if ($this->math >= 90 && $this->eng >= 90 && $this->ben >= 90 && $this->phy >= 90) {
         echo "Golden A+";
     } elseif ($this->math >= 80 && $this->eng >= 80 && $this->ben >= 80 && $this->phy >= 80) {
         echo "A+";
     } elseif ($this->math >= 70 && $this->eng >= 70 && $this->ben >= 70 && $this->phy >= 70) {
         echo "A";
     } elseif ($this->math < 1 || $this->eng < 1 || $this->ben < 1 || $this->phy < 1) {
         echo "Fail";
     } else {
         echo "Pass";
     }
 }
    public function getTotal(){
        return $this->math + $this->eng + $this->ben + $this->phy;
     }

}
$math = $_POST['math'];
$eng = $_POST['eng'];
$ben = $_POST['ben'];
$phy = $_POST['phy'];
$obj = new result();
if ($math == NULL || $eng == NULL || $ben == NULL || $phy == NULL){
    echo "Please Complete all Filds";
}elseif ($math > 100 || $eng > 100 || $ben > 100 || $phy > 100){
    echo "Number Can't be over 100";
}
elseif ($math < 0 || $eng < 0 || $ben < 0 || $phy < 0){
    echo "Number Can't be less than 0";
} else {
    $obj->setValue($math, $eng, $ben, $phy);
    echo "Result is: ";
    $obj->getResult();
    echo "<br/>Total: ";
    echo $obj->getTotal();

}

